# If no Docker image name provided, use default value
DOCKER_IMG ?= qcp_innsbruck_public/ci_base_image

.PHONY: build
build:
	$(info * Building Docker image '$(DOCKER_IMG)'...)
	docker build --pull \
	             -t $(DOCKER_IMG) \
	             .

.PHONY: push
push:
	$(info * Pushing Docker image '$(DOCKER_IMG)'...)
	docker push $(DOCKER_IMG)
