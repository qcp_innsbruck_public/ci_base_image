FROM ubuntu:19.10

# Disable interactive dialogues in debconf/apt
ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update -q \
 && apt-get install --no-install-recommends -q -y \
    ansible \
    build-essential \
    ca-certificates \
    docker.io \
    git \
    gnupg2 \
    libmysqlclient-dev \
    libpq-dev \
    make \
    pass \
    python3 \
    python3-dev \
    python3-venv \
    unzip \
    wget \
 && apt-get clean -q \
 && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Install Terraform
#  Get link to current Terraform ZIP Archive from: https://www.terraform.io/downloads.html
ARG TERRAFORM_ZIP=/tmp/terraform.zip
ARG TERRAFORM_VERSION=0.12.24
RUN wget \
    https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip \
    -O ${TERRAFORM_ZIP} \
    --quiet \
 && unzip ${TERRAFORM_ZIP} -d /tmp \
 && rm ${TERRAFORM_ZIP} \
 && mv /tmp/terraform /usr/local/bin
